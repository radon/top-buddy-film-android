package it.airbagstudio.topfilmbuddy

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class FilmsRecyclerAdapter(var films: List<Film>, val listener: OnFilmInteractionListener): RecyclerView.Adapter<FilmsRecyclerAdapter.FilmHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.film_row, parent, false)
        return FilmHolder(view)
    }

    override fun getItemCount(): Int = films.count()

    override fun onBindViewHolder(holder: FilmHolder, position: Int) {
        val film = films[position]
        Glide.with(holder.picture.context).load("https://image.tmdb.org/t/p/w500/${film.poster_file}").into(holder.picture)
        holder.title.text = film.title
        holder.overview.text = film.overview
        holder.itemView.setOnClickListener {
            listener.filmSelected(film)
        }
    }

    class FilmHolder(view: View): RecyclerView.ViewHolder(view) {
        val picture: ImageView = view.findViewById(R.id.picture)
        val title: TextView = view.findViewById(R.id.title)
        val overview: TextView = view.findViewById(R.id.overview)
    }


}