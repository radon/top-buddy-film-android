package it.airbagstudio.topfilmbuddy

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.activity_films.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

interface OnFilmInteractionListener {
    fun filmSelected(film: Film)
}

class FilmsActivity : AppCompatActivity(), OnFilmInteractionListener {

    val retrofit = Retrofit.Builder()
        .baseUrl("https://www.airbagstudio.it")
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    val filmApi = retrofit.create(FilmApiInterface::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_films)

        viewPager.adapter = PagerAdapter(supportFragmentManager, this)
        tabLayout.setupWithViewPager(viewPager)

        supportActionBar?.elevation = 0.0f

        val sp = getSharedPreferences(
        getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        val editor = sp.edit()

        if (sp.getString("films", "null").isNullOrBlank()) {
            filmApi.getMovies().enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>, t: Throwable) {
                    Log.e("FilmsActivity", "onFailure", t)
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    if (response.isSuccessful) {
                        response.body().let {

                            editor.putString("films", it)
                            editor.apply()
                        }
                    }
                }
            })
        }

    }

    override fun filmSelected(film: Film) {
        val intent = Intent(this, FilmDetailActivity::class.java)
        intent.putExtra("title", film.title)
        startActivity(intent)
    }

    class PagerAdapter(supportFragmentManager: FragmentManager, private val context: Context): FragmentPagerAdapter(supportFragmentManager) {
        override fun getCount(): Int = 2

        override fun getItem(position: Int): Fragment =
            FilmListFragment.newInstance(position == 1)

        override fun getPageTitle(position: Int): CharSequence? =
            context.getString(if (position == 0) R.string.top_movies else R.string.favourites)
        

    }
}
