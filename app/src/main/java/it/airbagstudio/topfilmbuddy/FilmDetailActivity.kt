package it.airbagstudio.topfilmbuddy

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.view.get
import com.bumptech.glide.Glide
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import kotlinx.android.synthetic.main.activity_film_detail.*

class FilmDetailActivity : AppCompatActivity() {

    private var currentFilm: Film? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_film_detail)

        intent?.let {
            if (it.hasExtra("title")) {
                val paramTitle = it.getStringExtra("title")

                val sp = getSharedPreferences(
                    getString(R.string.preference_file_key), Context.MODE_PRIVATE
                )
                sp.getString("films", "[]")?.let { json ->

                    parseFilms(json)?.let { films ->

                        films.filter { it.title == paramTitle }.firstOrNull()?.let { film ->
                            currentFilm = film
                            title = film.title
                            Glide.with(this).load("https://image.tmdb.org/t/p/w500/${film.poster_file}").into(film_picture)
                            filmTitle.text = film.title
                            filmCast.text = film.cast.joinToString(", ")
                            filmGenres.text = film.genres.joinToString(", " )
                            filmDirector.text = film.director
                            filmOverview.text = film.overview
                        }

                    }
                }
            } else {
                finish()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menu?.let { unwrappedMenu ->
            currentFilm?.let { film ->
                unwrappedMenu.add("")
                val menuItem = unwrappedMenu.getItem(0)
                if (film.isFavorite) {
                    menuItem.setIcon( R.drawable.ic_favorite_on)
                } else {
                    menuItem.setIcon( R.drawable.ic_favorite_off)
                }
                menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
            }
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        currentFilm?.let { film ->
            val sp = getSharedPreferences(
                getString(R.string.preference_file_key), Context.MODE_PRIVATE
            )
            sp.getString("films", "[]")?.let { json ->
                parseFilms(json)?.let { films ->

                    val index = films.indexOfFirst { it.title == film.title }
                    films[index].isFavorite = !films[index].isFavorite

                    currentFilm = films[index]

                    val editor = sp.edit()
                    editor.putString("films", encodeFilms(films))
                    editor.apply()

                    if (films[index].isFavorite) {
                        item?.setIcon(R.drawable.ic_favorite_on)
                    } else {
                        item?.setIcon(R.drawable.ic_favorite_off)
                    }

                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
