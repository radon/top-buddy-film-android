package it.airbagstudio.topfilmbuddy

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

fun parseFilms(json: String): List<Film>? {
    val moshi = Moshi.Builder().build()
    val listType = Types.newParameterizedType(List::class.java, Film::class.java)
    val jsonAdapter: JsonAdapter<List<Film>> = moshi.adapter(listType)
    return jsonAdapter.fromJson(json)
}

fun encodeFilms(films: List<Film>): String? {
    val moshi = Moshi.Builder().build()
    val listType = Types.newParameterizedType(List::class.java, Film::class.java)
    val jsonAdapter: JsonAdapter<List<Film>> = moshi.adapter(listType)
    return jsonAdapter.toJson(films)
}