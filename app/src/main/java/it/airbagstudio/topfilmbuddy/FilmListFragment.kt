package it.airbagstudio.topfilmbuddy

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_film_list.*


private const val ARG_FAVORITE = "isFavorite"

class FilmListFragment : Fragment(),
    SharedPreferences.OnSharedPreferenceChangeListener,
    OnFilmInteractionListener {
    private var isFavorite: Boolean = false
    private var listener: OnFilmInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            isFavorite = it.getBoolean(ARG_FAVORITE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_film_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = FilmsRecyclerAdapter(emptyList(), this@FilmListFragment)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFilmInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFilmInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onStart() {
        super.onStart()
        val preferences = context?.getSharedPreferences(
            getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        preferences?.registerOnSharedPreferenceChangeListener(this)

        reloadFilms()
    }


    override fun onStop() {
        super.onStop()
        val preferences = context?.getSharedPreferences(
            getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        preferences?.unregisterOnSharedPreferenceChangeListener(this)
    }

    fun reloadFilms() {

        val preferences = context?.getSharedPreferences(
            getString(R.string.preference_file_key), Context.MODE_PRIVATE)

        preferences?.getString("films", "[]")?.let { json ->
            parseFilms(json).let { films ->
                if (recyclerView.adapter is FilmsRecyclerAdapter && films != null) {
                    val recyclerAdapter = recyclerView.adapter as FilmsRecyclerAdapter
                    val filteredFilms = if (isFavorite) films.filter { it.isFavorite } else films
                    recyclerAdapter.films = filteredFilms
                    recyclerAdapter.notifyDataSetChanged()
                }
            }
        }

    }

    override fun filmSelected(film: Film) {
        listener?.filmSelected(film)
    }

    override fun onSharedPreferenceChanged(sp: SharedPreferences?, key: String?) {
        reloadFilms()
    }

    companion object {
        @JvmStatic
        fun newInstance(isFavorite: Boolean) =
            FilmListFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(ARG_FAVORITE, isFavorite)
                }
            }
    }
}
