package it.airbagstudio.topfilmbuddy

data class Film(
    val title: String,
    val overview: String,
    val release_date: String,
    val genres: List<String>,
    val director: String,
    val cast: List<String>,
    val poster_file: String,
    var isFavorite: Boolean = false
)