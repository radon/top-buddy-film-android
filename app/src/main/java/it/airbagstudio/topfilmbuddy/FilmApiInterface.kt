package it.airbagstudio.topfilmbuddy

import retrofit2.Call
import retrofit2.http.GET

interface FilmApiInterface {

    @GET("TopFilmBuddy.json")
    fun getMovies(): Call<String>

}